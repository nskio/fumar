# frozen_string_literal: true

source "https://rubygems.org"

git_source(:github) { |repo_name| "https://github.com/#{repo_name}" }

# gem "rails"

# Routing Tree Web Toolkit
gem "roda", "~> 3.48"

# Using puma as the app server
gem "puma", "~> 5.5"

# Make-like build util for Ruby
gem "rake", "~> 13.0"

# Sequel: The Database Toolkit for Ruby
gem "sequel", "~> 5.49"

# Faster SELECTs when using Sequel with pg
gem "sequel_pg", "~> 1.14"

# Runtime dev console and IRB alt
gem "pry", "~> 0.14.1"

# Documentation tool
gem "yard", "~> 0.9.26"

# Fast JSON parser and object marshaller
gem "oj", "~> 3.13"

# Ruby binding for OpenBSD bcrypt() password hashing algo,
# allowing you to easily store a secure hash of user pass
gem "bcrypt", "~> 3.1"

# Logger for Roda with some tricks
gem "roda-enhanced_logger", "~> 0.4.0"

# Organizes code into reusable components
gem "dry-system", "~> 0.20.0"

# Toolkit of support libs and Ruby core extensions extracted from
# rails framework.
gem "activesupport", "~> 6.1"

# Plugin that adds BCrypt auth and pass hashing to Sequel models
gem "sequel_secure_password", "~> 0.2.15"

# Efficient and thread-safe code loader
gem "zeitwerk", "~> 2.4"

gem "dry-validation", "~> 1.7"

# Required I think for the named TZ with Sequel
gem "tzinfo", "~> 2.0"

# Will annotate the models with the schema from the migration
gem "sequel-annotate", "~> 1.7"

group :development, :test do
  # Loads env vars from .env
  gem "dotenv", "~> 2.7", :group => :development
  gem "rspec", "~> 3.10", :group => :development
  gem "rack-test", "~> 1.1", :group => :development
end

################################################################################
# Leftover gems from the book that we probably want to pull in at a later point
# in time.
################################################################################


# [Test]
# Sets up ruby obj as test data
#gem "factory_bot", "~> 6.2", :group => :development
# Provides "time travel" and "time freezing" capabiliies, making it
# dead simple to test time-dependent code. Provides a unified method
# to mock Time.now, Date.today, and DateTime.now in single call.
#gem "timecop", "~> 0.9.4"

# [Linting]
# Static code analyzer and formatter based on the community style
# guide
#gem "rubocop", "~> 1.22", :group => :development
#gem "rubocop-performance", "~> 1.11", :group => :development
#gem "rubocop-rspec", "~> 2.5", :group => :development
#gem "rubocop-sequel", "~> 0.3.3", :group => :development
#gem "rubocop-rake", "~> 0.6.0", :group => :development

# [Internationalization]
# Internationalization and localization
#gem "i18n", "~> 1.8"

gem "pry-byebug", "~> 3.8"

gem "terminal-table", "~> 3.0"
