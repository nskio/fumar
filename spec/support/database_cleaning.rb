# It's generally best to run each test in its transaction if possible
# Keeps all tests isolated from eachother and it's simple as it handles all the
# cleanups for you.
RSpec.configure do |config|
  config.around do |example|
    Application['database'].transaction(rollback: :always, auto_savepoint: true) do
      example.run
    end
  end
end
