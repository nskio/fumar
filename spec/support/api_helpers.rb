# {APIHelpers} module contains helper methods that are used in the API request specs
module ApiHelpers
  # Returns the response that our request has returned
  def response
    last_response
  end

  # Parse the response JSON document into a Ruby data struct and return it
  def json_response
    JSON.parse(response.body)
  end
end

RSpec.configure do |config|
  config.include ApiHelpers
end
