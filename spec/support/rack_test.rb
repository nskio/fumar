# frozen_string_literal: true

require 'rack/test'

RSpec.configure do |config|
  # Includes the Rack::Test::Methods module for request type tests
  # The app method is required to launch the app during tests
  config.include Rack::Test::Methods, type: :request

  def app
    App.freeze.app
  end
end
