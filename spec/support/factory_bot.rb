# File for configuring FactoryBot, the lib for setting up Ruby obj as test dat
require 'factory_bot'

# Load all factories defined in spec/factories dir
FactoryBot.find_definitions

# By default, creating a record will call save! on the instance; since this is
# not always ideal, you can override the behavior by defining to_create on the
# factory:
# Out of the box FB doesn't work with Sequel, because save! literally isn't
# implemented
FactoryBot.define do
  to_create(&:save)
end

# Allow factory associations to follow the parent's build strategy.
# Previously, all factory associations were created, regardless of whether the
# parent was persisted to the database.
FactoryBot.use_parent_strategy = false

RSpec.configure do |config|
  config.include FactoryBot::Syntax::Methods
end
