require 'yaml'
if ARGV.length != 1
  puts "ERROR: Should accept single arg with smoke file name to total"
  exit 1
end

smokefile_name = ARGV[0]
puts "Smoke file name: #{smokefile_name}"
smokefile = File.join(__dir__, smokefile_name)

puts "Totalling smoke file: #{smokefile}"

hash = YAML.load(File.read(smokefile))

r = hash["rating"]
total =
  r["presentation"]["wrapper"] +
  r["presentation"]["band"] +
  r["presentation"]["firmness"] +
  r["presentation"]["oils"] +
  r["smoking_character"]["light"] +
  r["smoking_character"]["burn"] +
  r["smoking_character"]["draw"] +
  r["flavor"]["consistency"] +
  r["flavor"]["flavor_qty"] +
  r["flavor"]["smoothness"] +
  r["overall_impression"]["quality_of_blend"] +
  r["overall_impression"]["flavor_progression"] +
  r["overall_impression"]["finish"] +
  r["overall_impression"]["speed_of_burn"] +
  r["overall_impression"]["price"]

puts "Writing total: #{total}"

hash["rating"]["total"] = total

File.open(smokefile, "w") { |file| file.write(hash.to_yaml) }
