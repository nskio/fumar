#!/bin/bash
_dir="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

fileToValidate=$1

if [[ "$fileToValidate" == "" ]]; then
  echo "ERROR: Must provide file to validate as first arg"
  exit 1
fi

cd $_dir
yamale -s $_dir/schema.yml $1
