#!/bin/bash
_dir="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

cmd=$1
name=$2
purchaseDate=$3
dateStr=$(date +%m%d%Y)

containsElem() {
  local e match="$1"
  shift
  for e; do [[ "$e" == "$match" ]] && return 0; done
  return 1
}

valid_cmds=("order" "smoke")

containsElem "$cmd" "${valid_cmds[@]}"
result=$?

if [[ $result -eq 1 ]]; then
  echo "ERROR: Must provide a valid cmd"
  exit 1
fi

if [[ "$name" == "" ]]; then
  echo "ERROR: Must provide a filename suffix"
  exit 1
fi


function genNewOrder() {
  if [[ "$purchaseDate" == "" ]]; then
    echo "ERROR: Must provide a purchase date as last arg, format: mmddyyyy"
    exit 1
  fi

  filename="${purchaseDate}_${name}.yml"
  filePath="$_dir/orders/$filename"
  cp -f $_dir/orders/template.yml $filePath
  sed -i "s/purchase_date:/purchase_date: $purchaseDate/" $filePath
}

function genNewSmoke() {
  filename="${dateStr}_${name}.yml"
  filePath="$_dir/smokes/$filename"
  cp -f $_dir/smokes/template.yml $filePath
  sed -i "s/date:/date: $dateStr/" $filePath
}

case "$cmd" in

  "order")
    genNewOrder
    ;;

  "smoke")
    genNewSmoke
    ;;
esac

