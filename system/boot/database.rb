# frozen_string_literal: true

Application.boot(:database) do |container|
  # Load environment variables before setting up database connection
  use :environment_variables

  init do
    require 'sequel/core'
  end

  start do
    puts "Loading database URL: #{ENV['DATABASE_URL']}"
    # Delete DATABASE_URL from environment, so it isn't accidentally passed to
    # subprocesses
    database = Sequel.connect(ENV.delete('DATABASE_URL'))

    # Register database component
    container.register(:database, database)
  end
end
