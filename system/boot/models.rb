# frozen_string_literal: true
#
# This file contains config for Sequel Models

Application.boot(:models) do
  init do
    require 'sequel/model'
  end

  start do
    # Whether association metadata should be cached in the assiciation reflection.
    # If not cached, it will be computed on demand.
    # Generally, only want this false when using code reloading. Ensures the
    # associated class is removed or modified it's updated
    Sequel::Model.cache_associations = false if Application.env == 'development'

    # validation_helpers plugin contains validate_* methods designed to be called
    # inside Model#validate to perform validations.
    Sequel::Model.plugin(:auto_validations)
    Sequel::Model.plugin(:validation_helpers)

    # The prepared statements plugin modifies the model to use prepared statements
    # for instance level inserts and updates.
    Sequel::Model.plugin(:prepared_statements)

    # Allows easy access all model subclasses and descendent classes
    Sequel::Model.plugin(:subclasses) unless Application.env == 'development'

    # Adds hooks for auto setting create and update timestamps
    Sequel::Model.plugin(:timestamps)

    # Allows for named timezones instead of :local and :utc (requires TZInfo)
    Sequel.extension(:named_timezones)

    # Use UTC for saving time inside db
    Sequel.default_timezone = :utc

    # Freeze all descendent clases. Also finalizes the associations for those
    # classes before freezing.
    Sequel::Model.freeze_descendents unless Application.env == 'development'
  end
end
