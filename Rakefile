# frozen_string_literal: true
#

require_relative './system/application'

# Enable database component
Application.start(:database)

# Enable logger component
Application.start(:logger)

#Application['database'].loggers << Application['logger']

migrate =
  lambda do |version|
    Sequel.extension(:migration)

    # Perform migrations based on migration files in specified dir
    Sequel::Migrator.apply(Application['database'], 'db/migrate', version)

    # Dump the schema after the migration
    Rake::Task['db:dump'].invoke
  end

namespace :models do
  desc 'Annotate the models with schema information'
  task :annotate do
    require 'sequel/annotate'
    Sequel::Annotate.annotate(Dir['models/*.rb'])
  end
end

namespace :db do
  desc 'Migrate the database'
  task :migrate do
    migrate.call(nil)
  end

  desc 'Rolling back latest migration'
  task :rollback do |_, _args|
    current_version = Application['database'].fetch('SELECT * FROM schema_info').first[:version]

    migrate.call(current_version - 1)
  end

  desc 'Dump the database schema to a file'
  task :dump do
    development = Application.env == 'development'

    sh %(pg_dump --schema-only --no-privileges --no-owner -s #{Application['database'].url} > db/structure.sql) if development
  end

  desc 'Seed database with test data'
  task :seed do
    sh %(ruby db/seeds.rb)
  end

  desc 'Generate project documentation using yard'
  task :docs do
    sh %(yard doc *.rb app/ lib/)
  end

end

task :import do
  #require 'securerandom'
  #require 'dry-validation'

  # Register automatically application classes and external deps from the
  # /system/boot folder
  Application.finalize!

  log = Application['logger']
  log.info "Hello world."

  user = User.find_or_create(
    username: 'eriknelson',
    email: 'erik@nsk.io',
    password_digest: 'foo',
    authentication_token: 'bar',
  )

  project_dir = File.absolute_path(File.dirname(__FILE__))
  log.info "Project dir: #{project_dir}"

  if !ENV['SKIP_TSV_IMPORT']
    tsv_importer = Importers::Tsv.new(log, user,
      File.join(project_dir,'dat','purchase_history.tsv'),
      File.join(project_dir,'dat','full_inventory.tsv'),
    )
    tsv_importer.run
  end

  if !ENV['SKIP_DAT_IMPORT']
    orders_dat_importer = Importers::OrderDatImporter.new(
      File.join(project_dir,'dat','orders'),
      user,
    )
    orders_dat_importer.run
  end

end

task :table do
  require 'terminal-table'

  # Register automatically application classes and external deps from the
  # /system/boot folder
  Application.finalize!

  log = Application['logger']

  user = User.find_or_create(
    username: 'eriknelson',
    email: 'erik@nsk.io',
    password_digest: 'foo',
    authentication_token: 'bar',
  )

  project_dir = File.absolute_path(File.dirname(__FILE__))
  log.info "Project dir: #{project_dir}"

  headings = [
    'Arrival Date', 'Maturity Date', 'Lot', 'Length', 'Gauge', 'Inv', 'PPS'
  ]

  table = Terminal::Table.new :headings => headings do |t|
    total_counter = 0
    PurchaseLot.all.each do |pl|
      next if pl.inventory_count == 0
      total_counter += pl.inventory_count
      c = pl.cigar
      lot = "#{c.blend.brand.name} #{c.blend.name} #{c.wrapper.name} #{c.vitola.name}"
      maturity_date = pl.arrival_date + 14
      t << [
        pl.arrival_date, maturity_date, lot, c.vitola.length, c.vitola.gauge,
        pl.inventory_count, pl.product != nil ? "$#{pl.pps}" : "No Product",
      ]
    end

    t << :separator
    t << ["Total Inventory Count: #{total_counter}"]
  end

  puts table
end

task :debug do
  # Register automatically application classes and external deps from the
  # /system/boot folder
  Application.finalize!

  log = Application['logger']

  user = User.find_or_create(
    username: 'eriknelson',
    email: 'erik@nsk.io',
    password_digest: 'foo',
    authentication_token: 'bar',
  )

  project_dir = File.absolute_path(File.dirname(__FILE__))
  log.info "Project dir: #{project_dir}"

  Product.all.each do |pd|
    log.info "Product: #{pd.name}"
    pd.purchase_lots.each do |pl|
      log.info "-> #{pl.cigar.blend.brand.name} #{pl.cigar.blend.name}, PPS: $#{pl.pps}"
    end
  end

  PurchaseLot.all.select{|p| p.product == nil}.each do |p|
    log.info "PurchaseLot missing product: [#{p.cigar.blend.brand.name} #{p.cigar.blend.name}]"
  end
end
