class PurchaseLot< Sequel::Model
  many_to_one :product
  many_to_one :cigar

  one_to_many :smokes

  many_to_many :humidors

  def price_per_stick
    (self.product.price / self.product.purchase_count).round(2)
  end

  def pps
    self.price_per_stick
  end
end
