class Humidor < Sequel::Model
  many_to_one :user
  many_to_many :purchase_lots
end
