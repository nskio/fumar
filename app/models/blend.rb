class Blend < Sequel::Model
  many_to_one :brand

  one_to_many :cigars
end
