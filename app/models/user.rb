class User < Sequel::Model
  # Plugin that adds BCrypt authentication and password hashing to Sequel models
  # Provided by the sequel_secure_password gem.
  plugin :secure_password

  # Adds additional formatting validation for the user email.
  def validate
    super

    validates_format(Constants::EMAIL_REGEX, :email) if email
  end

  one_to_many :humidors
  one_to_many :orders
end
