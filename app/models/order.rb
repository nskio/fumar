class Order < Sequel::Model
  many_to_one :user
  many_to_one :vendor

  one_to_many :products

  def total_price
    (self.order_total + self.shipping_cost + self.tax_cost).round(2)
  end
end
