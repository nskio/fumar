class Product < Sequel::Model
  many_to_one :order

  one_to_many :purchase_lots
end
