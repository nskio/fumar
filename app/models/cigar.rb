class Cigar < Sequel::Model
  many_to_one :blend
  many_to_one :vitola
  many_to_one :wrapper

  one_to_many :purchase_lots
end
