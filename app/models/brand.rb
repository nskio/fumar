class Brand < Sequel::Model
  one_to_many :blends

  # Utility associations
  #has_many :cigars, :through => :blends
end
