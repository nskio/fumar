Sequel.migration do
  transaction

  change do

    create_table(:humidors) do
      primary_key :id

      column :name, String, null: false

      foreign_key :user_id, :users

      column :created_at, DateTime, null: false, default: Sequel::CURRENT_TIMESTAMP
      column :updated_at, DateTime, null: false, default: Sequel::CURRENT_TIMESTAMP
    end

    create_table(:humidors_purchase_lots) do
      primary_key :id

      foreign_key :humidor_id, :humidors
      foreign_key :purchase_lot_id, :purchase_lots

      column :created_at, DateTime, null: false, default: Sequel::CURRENT_TIMESTAMP
      column :updated_at, DateTime, null: false, default: Sequel::CURRENT_TIMESTAMP
    end
  end
end
