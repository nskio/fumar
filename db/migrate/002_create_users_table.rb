Sequel.migration do
  change do
    create_table(:users) do
      primary_key :id

      column :username, String, null: false

      # Note: citext module provides a case-insensitive char string type, citext.
      # Essentially, it internally calls lower when comparing values. Otherwise,
      # it's basically text.
      column :email, 'citext', null: false, unique: true

      # Stores a user hashed password
      column :password_digest, String, null: false

      # Unique token among all users that is used during auth verifications.
      # It's necessary to invalidate existing auth tokens when the user logs out
      # or wants to refresh their token.
      column :authentication_token, String, null: false, unique: true
      column :created_at, DateTime, null: false, default: Sequel::CURRENT_TIMESTAMP
      column :updated_at, DateTime, null: false, default: Sequel::CURRENT_TIMESTAMP
    end
  end
end
