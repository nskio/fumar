Sequel.migration do
  transaction

  change do
    create_table(:vendors) do
      primary_key :id

      column :name, String, null: false
      column :link, String

      column :created_at, DateTime, null: false, default: Sequel::CURRENT_TIMESTAMP
      column :updated_at, DateTime, null: false, default: Sequel::CURRENT_TIMESTAMP
    end

    create_table(:brands) do
      primary_key :id

      column :name, String, null: false
      column :link, String

      column :created_at, DateTime, null: false, default: Sequel::CURRENT_TIMESTAMP
      column :updated_at, DateTime, null: false, default: Sequel::CURRENT_TIMESTAMP
    end

    create_table(:blends) do
      primary_key :id

      column :name, String, null: false
      column :body, String, null: false
      column :country, String, null: false

      foreign_key :brand_id, :brands

      column :created_at, DateTime, null: false, default: Sequel::CURRENT_TIMESTAMP
      column :updated_at, DateTime, null: false, default: Sequel::CURRENT_TIMESTAMP
    end

    create_table(:vitolas) do
      primary_key :id

      column :name, String, null: false
      column :length, Float, null: false
      column :gauge, Integer, null: false

      column :created_at, DateTime, null: false, default: Sequel::CURRENT_TIMESTAMP
      column :updated_at, DateTime, null: false, default: Sequel::CURRENT_TIMESTAMP
    end

    create_table(:wrappers) do
      primary_key :id

      column :name, String, null: false
      column :description, String

      column :created_at, DateTime, null: false, default: Sequel::CURRENT_TIMESTAMP
      column :updated_at, DateTime, null: false, default: Sequel::CURRENT_TIMESTAMP
    end

    create_table(:cigars) do
      primary_key :id

      foreign_key :blend_id, :blends
      foreign_key :vitola_id, :vitolas
      foreign_key :wrapper_id, :wrappers

      column :created_at, DateTime, null: false, default: Sequel::CURRENT_TIMESTAMP
      column :updated_at, DateTime, null: false, default: Sequel::CURRENT_TIMESTAMP
    end

    create_table(:orders) do
      primary_key :id

      column :purchase_date, Date, null: false
      column :shipping_cost, BigDecimal, size: [10,2], null: false
      column :tax_cost, BigDecimal, size: [10,2], null: false
      column :order_total, BigDecimal, size: [10,2], null: false

      foreign_key :vendor_id, :vendors
      foreign_key :user_id, :users

      column :created_at, DateTime, null: false, default: Sequel::CURRENT_TIMESTAMP
      column :updated_at, DateTime, null: false, default: Sequel::CURRENT_TIMESTAMP
    end

    create_table(:products) do
      primary_key :id

      column :price, BigDecimal, size: [10,2], null: false
      column :name, String, null: false
      column :purchase_count, Integer, null: false

      foreign_key :order_id, :orders

      column :created_at, DateTime, null: false, default: Sequel::CURRENT_TIMESTAMP
      column :updated_at, DateTime, null: false, default: Sequel::CURRENT_TIMESTAMP
    end

    create_table(:purchase_lots) do
      primary_key :id

      column :purchase_date, Date
      column :arrival_date, Date
      column :inventory_count, Integer, null: false
      column :purchase_count, Integer, null: false

      # NOTE: It's possible that this can be NULL in the event that there's a
      # purchase lot that just doesn't have a product that's been logged to
      # be associated. It's basically an orphaned purchase lot.
      foreign_key :product_id, :products, null: true
      foreign_key :cigar_id, :cigars

      column :created_at, DateTime, null: false, default: Sequel::CURRENT_TIMESTAMP
      column :updated_at, DateTime, null: false, default: Sequel::CURRENT_TIMESTAMP
    end
  end
end
