Sequel.migration do
  transaction

  change do

    create_table(:smokes) do
      primary_key :id

      # Presentation
      column :wrapper_rating, Integer, null: false
      column :band_rating, Integer, null: false
      column :firmness_rating, Integer, null: false
      column :oils_rating, Integer, null: false

      # Smoking Character
      column :light_rating, Integer, null: false
      column :burn_rating, Integer, null: false
      column :draw_rating, Integer, null: false

      # Flavor
      column :consistency_rating, Integer, null: false
      column :flavor_quantity_rating, Integer, null: false
      column :smoothness_rating, Integer, null: false

      # Overall Impressino
      column :quality_of_blend_rating, Integer, null: false
      column :flavor_progression_rating, Integer, null: false
      column :finish_rating, Integer, null: false
      column :speed_of_burn_rating, Integer, null: false
      column :price_rating, Integer, null: false

      column :notes, String, text: true, null: false

      foreign_key :purchase_lot_id, :purchase_lots

      column :created_at, DateTime, null: false, default: Sequel::CURRENT_TIMESTAMP
      column :updated_at, DateTime, null: false, default: Sequel::CURRENT_TIMESTAMP
    end

  end
end
