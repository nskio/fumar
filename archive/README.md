# fumar

## Setting up env

You'll need a linux environment or mac. Suggestion is to use the Ubuntu app for
Windows.

Install [RVM](https://rvm.io/rvm/install#basic-install)

Install Ruby 3.0.0

`rvm install 3.0.0`

Create a gemset to isolate the fumar environment and dependencies

`rvm gemset create fumar`

The fumar project has a `.ruby-version` file at the top level of the project
that tells rvm to use the `3.0.0@fumar` gemset when the project becomes your
active working directory.

Navigate to the fumar directory which activates your gemset

`cd $YOUR_CHECKOUT_DIR/fumar`

Install bundler, used to manage our dependencies

`gem install bundler`

Install the dependencies

`bundler install`

There is a local config file used for your local development settings.
Copy the example to its own file, and you're free to edit as you please.
This is required.

`cp config.sh.ex config.sh`

## Running

The executable `fumar` is the entrypoint for the ruby server. It's preferable
to start using the wrapper script `run.sh`, which sets some environment configuration
and other various parameters for startup like a database location.

`./run.sh`

> NOTE: By default, run.sh will start in "dev" mode. The explicit envirionment
> can be provided as a first positional argument: `run.sh prod` for example.
