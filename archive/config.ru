$LOAD_PATH.unshift(
  File.join(File.absolute_path(File.dirname(__FILE__)), 'srv')
)

require 'roda'
require 'logging'
require 'dry-auto_inject'
require 'dry-container'
require 'pry'
require 'active_record'
require 'fumar/models'

def setup_dev_db_cx(db_file)
  ActiveRecord::Base.establish_connection(
    adapter: 'sqlite3',
    database: db_file,
  )
end

def setup_logger
  Logging.color_scheme(
    'bright', :levels => {
      :info  => :green,
      :warn  => :yellow,
      :error => :red,
      :fatal => [:white, :on_red]
    },
    :date => :blue,
  )

  Logging.appenders.stdout(
    'stdout',
    :layout => Logging.layouts.pattern(
      :pattern => '[%d] %-5l %m\n',
      :color_scheme => 'bright'
    )
  )

  log = Logging.logger['Fumar']
  log.add_appenders 'stdout'
  log
end

def load_config
  {
    engine: {
      foo: 'bar'
    },
    em_threadpool_size: 20 # 20 is default, just showing it's configurable
  }
end

_config = load_config()
master_deps = Dry::Container.new
log = setup_logger()
master_deps.register(:log, log)
master_deps.register(:config, _config)
InjectMasterDeps = Dry::AutoInject(master_deps)

db_type = ENV['DB_TYPE']
case db_type
when 'sqlite'
  db_file = ENV['DB_FILE']
  log.debug "Establishing sqlite connection to file: #{db_file}"
  setup_dev_db_cx(db_file)
else
  log.error "Did not recognize db type, failed startup"
  exit 1
end

class FumarApp < Roda
  plugin :json
  route do |r|
    r.root do
      ::Fumar::Models::Cigar.all.as_json(
        include: [:brand, :blend, :wrapper, :vitola]
      )
    end
  end
end

run FumarApp.freeze.app
