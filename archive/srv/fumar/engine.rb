module Fumar
  class Engine < Fumar::Machinery::Base
    def initialize()
      super() # IMPORTANT: Must call super to get deps
      @config = self.config[:engine]
      self.log.debug 'Engine#initialize'
    end
    def start
      self.log.debug 'Engine#start'
    end
  end
end
