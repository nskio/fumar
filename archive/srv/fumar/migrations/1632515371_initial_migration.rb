module Fumar
  module Migrations

    class CreateUserTable < ActiveRecord::Migration[6.0]
      def change
        unless ActiveRecord::Base.connection.table_exists?(:users)
          create_table :users do |t|
            t.string :username
            t.timestamps
          end
        end
      end
    end

    class CreateIdentityTable < ActiveRecord::Migration[6.0]
      def change
        unless ActiveRecord::Base.connection.table_exists?(:identities)
          create_table :identities do |t|
            t.string :provider
            t.references :user, foreign_key: true
          end
        end
      end
    end

    class CreateVendorTable < ActiveRecord::Migration[6.0]
      def change
        unless ActiveRecord::Base.connection.table_exists?(:vendors)
          create_table :vendors do |t|
            t.string :name
            t.string :link
            t.timestamps
          end
        end
      end
    end

    class CreateBrandTable < ActiveRecord::Migration[6.0]
      def change
        unless ActiveRecord::Base.connection.table_exists?(:brands)
          create_table :brands do |t|
            t.string :name
            t.string :link
            t.timestamps
          end
        end
      end
    end

    class CreateBlendTable < ActiveRecord::Migration[6.0]
      def change
        unless ActiveRecord::Base.connection.table_exists?(:blends)
          create_table :blends do |t|
            t.string :name
            t.string :body
            t.string :country
            t.references :brand, foreign_key: true
            t.timestamps
          end
        end
      end
    end

    class CreateVitolaTable < ActiveRecord::Migration[6.0]
      def change
        unless ActiveRecord::Base.connection.table_exists?(:vitolas)
          create_table :vitolas do |t|
            t.string :name
            t.float :length
            t.integer :gauge
            t.timestamps
          end
        end
      end
    end

    class CreateWrapperTable < ActiveRecord::Migration[6.0]
      def change
        unless ActiveRecord::Base.connection.table_exists?(:wrappers)
          create_table :wrappers do |t|
            t.string :name
            t.text :description
            t.timestamps
          end
        end
      end
    end

    class CreateCigarTable < ActiveRecord::Migration[6.0]
      def change
        unless ActiveRecord::Base.connection.table_exists?(:cigars)
          create_table :cigars do |t|
            t.references :blend, foreign_key: true
            t.references :vitola, foreign_key: true
            t.references :wrapper, foreign_key: true
            t.timestamps
          end
        end
      end
    end

    class CreatePurchaseLotTable < ActiveRecord::Migration[6.0]
      def change
        unless ActiveRecord::Base.connection.table_exists?(:purchase_lots)
          create_table :purchase_lots do |t|
            # maturity (virtual)
            # price_per_stick (virtual)
            # purchase_count (virtual)
            # inventory_value (virtual)

            t.datetime :arrival_date
            t.decimal :price
            t.integer :purchase_count
            t.integer :inventory_count

            t.references :order, foreign_key: true
            t.references :cigar, foreign_key: true
            t.timestamps
          end
        end
      end
    end

    class CreateOrderTable < ActiveRecord::Migration[6.0]
      def change
        unless ActiveRecord::Base.connection.table_exists?(:orders)
          create_table :orders do |t|
            # final_total (virtual)
            t.datetime :purchase_date
            t.decimal :product_total
            t.decimal :shipping_cost
            t.decimal :tax_cost
            t.references :vendor, foreign_key: true
            t.references :user, foreign_key: true
            t.timestamps
          end
        end
      end
    end

    class CreateHumidorTable < ActiveRecord::Migration[6.0]
      def change
        unless ActiveRecord::Base.connection.table_exists?(:humidors)
          create_table :humidors do |t|
            t.string :name
            t.timestamps
          end
        end
      end
    end

    class CreateHumidorsPurchaseLotsTable < ActiveRecord::Migration[6.0]
      def change
        unless ActiveRecord::Base.connection.table_exists?(:humidors_purchase_lots)
          create_table :humidors_purchase_lots do |t|
            # final_total (virtual)
            t.references :humidor, foreign_key: true
            t.references :purchase_lot, foreign_key: true
            t.timestamps
          end
        end
      end
    end

  end
end
