require 'active_record'

module Fumar
  module Models
    class Wrapper < ActiveRecord::Base
      has_many :cigars, dependent: :destroy
    end
  end
end
