require 'active_record'

module Fumar
  module Models
    class Humidor < ActiveRecord::Base
      belongs_to :user
      has_and_belongs_to_many :purchase_lots
    end
  end
end
