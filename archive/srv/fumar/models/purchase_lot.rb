require 'active_record'

module Fumar
  module Models
    class PurchaseLot < ActiveRecord::Base
      belongs_to :order
      belongs_to :cigar
      has_and_belongs_to_many :humidors

      def price_per_stick
        self.price / self.purchase_count
      end
    end
  end
end
