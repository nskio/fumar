require 'active_record'

module Fumar
  module Models
    class Blend < ActiveRecord::Base
      belongs_to :brand
      has_many :cigars, dependent: :destroy
    end
  end
end
