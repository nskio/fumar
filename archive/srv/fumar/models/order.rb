require 'active_record'

module Fumar
  module Models
    class Order < ActiveRecord::Base
      belongs_to :vendor
      belongs_to :user
      has_many :purchase_lots

      def total_price
        (self.product_total + self.shipping_cost + self.tax_cost).round(2)
      end
    end
  end
end
