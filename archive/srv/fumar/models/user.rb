require 'active_record'

module Fumar
  module Models
    class User < ActiveRecord::Base
      has_many :identities, dependent: :destroy
      has_many :orders, dependent: :destroy
      has_many :purchase_lots, :through => :orders
      has_many :cigars, :through => :purchase_lots
      has_many :humidors
    end
  end
end
