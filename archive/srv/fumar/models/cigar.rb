require 'active_record'

module Fumar
  module Models
    class Cigar < ActiveRecord::Base
      belongs_to :blend
      belongs_to :vitola
      belongs_to :wrapper
      has_many :purchase_lots
      has_one :brand, :through => :blend
    end
  end
end
