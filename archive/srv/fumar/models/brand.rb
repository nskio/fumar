require 'active_record'

module Fumar
  module Models
    class Brand < ActiveRecord::Base
      has_many :blends
      has_many :cigars, :through => :blends
    end
  end
end
