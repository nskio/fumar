require 'active_record'

module Fumar
  module Models
    class Identity < ActiveRecord::Base
      belongs_to :user
    end
  end
end
