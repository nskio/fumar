require 'active_record'

module Fumar
  module Models
    class Vendor < ActiveRecord::Base
      has_many :orders
    end
  end
end
