require 'active_record'

module Fumar
  def Fumar::setup_dev_db_cx(log, opt)
    db_type = opt[:db_type]
    case db_type
    when 'sqlite'
      db_file = opt[:db_file]
      log.debug "Establishing sqlite connection to file: #{db_file}"
      ActiveRecord::Base.establish_connection(
        adapter: 'sqlite3',
        database: db_file,
      )
    else
      log.error "Did not recognize db type, failed startup"
      exit 1
    end
  end
end
