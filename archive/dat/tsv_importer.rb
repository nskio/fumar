require_relative 'parse_orders'
require_relative 'parse_purchase_lots'

class TsvImporter
  def initialize(log, user, order_file, lot_file)
    @log = log
    @user = user
    @order_file = order_file
    @lot_file = lot_file

    @vendor_link_map = {
      'Cigars Daily'.parameterize.underscore.to_sym => 'https://cigarsdaily.com',
      'Famous Smoke'.parameterize.underscore.to_sym => 'https://www.famous-smoke.com',
      'Atlantic Cigar Company'.parameterize.underscore.to_sym => 'https://atlanticcigar.com',
      'Cigar King'.parameterize.underscore.to_sym => 'https://www.cigarking.com',
      'Cigar Bid'.parameterize.underscore.to_sym => 'https://www.cigarbid.com',
    }
  end

  def run()
    @log.debug "TsvImporter::run"

    @log.info "Parsing orders"
    parsed_orders = parse_orders(@log, @order_file)
    @log.info "Parsed #{parsed_orders.length} orders"

    @log.info "Saving orders and related vendors to database"
    self.save_orders parsed_orders

    @log.debug "Parsing purchase lots"
    parsed_purchase_lots = parse_purchase_lots(@log, @lot_file)
    @log.info "Parsed #{parsed_purchase_lots.length} purchase lots"

    @log.info "Saving purchase lots and related vendors to database"
    self.save_purchase_lots(parsed_purchase_lots)
  end

  private

  def save_orders(parsed_orders)
    imported_orders = []
    parsed_orders.each do |o|
      vendor_name = o[:vendor_name]
      vendor = Fumar::Models::Vendor.find_by(name: vendor_name)
      if not vendor
        @log.info "Vendor not found [ #{vendor_name} ], creating new one"
        vendor_link = @vendor_link_map[vendor_name.parameterize.underscore.to_sym]
        vendor = Fumar::Models::Vendor.create(
          name: vendor_name,
          link: vendor_link,
        )
        vendor.save
      end

      order = Fumar::Models::Order.find_by_id(o[:order_id])
      if not order
        @log.debug "Could not find order with id: #{o[:order_id]}, creating and saving"
        order = Fumar::Models::Order.create({
          purchase_date: o[:purchase_date],
          product_total: o[:product_total].round(2),
          shipping_cost: o[:shipping_cost].to_f.round(2),
          tax_cost: o[:tax_cost].to_f.round(2),
          user: @user,
          vendor: vendor,
        })
        order.save
      end

      imported_orders << order
    end

    @log.info "Saved #{imported_orders.length} orders"
    @log.debug "Order summary:"
    imported_orders.each { |o| @log.debug "Date: #{o.purchase_date}, Vendor: #{o.vendor.name}, Total: $#{o.total_price}, ID: #{o.id}" }
    imported_orders
  end

  def save_purchase_lots(parsed_purchase_lots)
    imported_purchase_lots = []
    parsed_purchase_lots.each do |pl|
      brand = Fumar::Models::Brand.find_by_name(pl.brand_name)
      if not brand
        @log.info "Brand not found [ #{pl.brand_name} ], creating new one"
        brand = Fumar::Models::Brand.create(
          name: pl.brand_name,
        )
        brand.save
      end

      blend = Fumar::Models::Blend.find_by_name(pl.blend_name)
      if not blend
        @log.info "Blend not found [ #{pl.blend_name} ], creating new one"
        blend = Fumar::Models::Blend.create(
          name: pl.blend_name,
          body: pl.blend_body,
          country: pl.blend_country,
          brand: brand,
        )
        blend.save
      end

      vitola = Fumar::Models::Vitola.find_by_name(pl.vitola_name)
      if not vitola
        @log.info "Vitola not found [ #{pl.vitola_name} ], creating new one"
        vitola = Fumar::Models::Vitola.create(
          name: pl.vitola_name,
          length: pl.vitola_length,
          gauge: pl.vitola_gauge,
        )
        vitola.save
      end

      wrapper = Fumar::Models::Wrapper.find_by_name(pl.wrapper_name)
      if not wrapper
        @log.info "Wrapper not found [ #{pl.wrapper_name} ], creating new one"
        wrapper = Fumar::Models::Wrapper.create(
          name: pl.wrapper_name,
        )
        wrapper.save
      end

      humidor_names = pl.humidor_name.split(/,/)
      pl_humidors = []
      humidor_names.each do |hn|
        humidor = Fumar::Models::Humidor.find_by_name(hn)
        if not humidor
          @log.info "Humidor not found [ #{pl.humidor_name} ], creating new one"
          humidor = Fumar::Models::Humidor.create(
            name: hn,
          )
          humidor.save
        end
        pl_humidors << humidor
      end

      cigar = Fumar::Models::Cigar.find_by(
        blend_id: blend.id,
        vitola_id: vitola.id,
        wrapper_id: wrapper.id,
      )
      if not cigar
        @log.info "Cigar [ #{brand.name} #{blend.name} #{wrapper.name} #{vitola.name} ] not found, creating new one"
        cigar = Fumar::Models::Cigar.create(
          blend: blend,
          vitola: vitola,
          wrapper: wrapper,
        )
        cigar.save
      end

      pl_price = pl.purchase_count * pl.price_per_stick
      purchase_lot = Fumar::Models::PurchaseLot.create(
        arrival_date: pl.arrival_date,
        price: pl.purchase_count * pl.price_per_stick,
        purchase_count: pl.purchase_count,
        inventory_count: pl.inventory_count,
        order: Fumar::Models::Order.find_by_id(pl.order_id),
        cigar: cigar,
      )

      purchase_lot.humidors = pl_humidors
      purchase_lot.save
    end

    #@log.info "Saved #{imported_orders.length} orders"
    #@log.debug "Order summary:"
    #imported_orders.each { |o| @log.debug "Date: #{o.purchase_date}, Vendor: #{o.vendor.name}, Total: $#{o.total_price}, ID: #{o.id}" }
    #imported_orders
  end
end
