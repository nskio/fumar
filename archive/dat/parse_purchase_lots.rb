require 'fumar/models'

ARRIVAL_DATE_IDX = 1
CIGAR_LINK_IDX = 3 # TODO
#VENDOR_NAME_IDX = 4 # Not necessary, comes off the order?
BRAND_NAME_IDX = 5
BLEND_NAME_IDX = 6
BLEND_BODY_IDX = 7
BLEND_COUNTRY_IDX = 9
VITOLA_NAME_IDX = 8
VITOLA_LENGTH_IDX = 11
VITOLA_GAUGE_IDX = 13
WRAPPER_NAME_IDX = 10
INVENTORY_COUNT_IDX = 15
HUMIDOR_NAME_IDX = 16
SMOKE_SCORE_IDX = 18
SMOKE_NOTES_IDX = 20
CIGAR_RATING_IDX = 19
FK_ORDER_ID_IDX = 21
PURCHASE_COUNT_IDX = 22
PRICE_PER_STICK_IDX= 14

class ImportedPurchaseLot
  attr_accessor \
    :arrival_date,
    :cigar_link,
    :vendor_name,
    :brand_name,
    :blend_name,
    :blend_body,
    :vitola_name,
    :blend_country,
    :wrapper_name,
    :vitola_length,
    :vitola_gauge,
    :inventory_count,
    :purchase_count,
    :price_per_stick,
    :humidor_name,
    :smoke_score,
    :cigar_rating,
    :smoke_notes,
    :order_id
end

def parse_purchase_lots(log, file)
  lots = []
  File.readlines(file).each do |line|
    pl = ImportedPurchaseLot.new
    split_line = line.split(/\t/)
    arrival_date = split_line[ARRIVAL_DATE_IDX]
    pl.arrival_date = DateTime.strptime(arrival_date,'%m/%d/%Y')
    pl.brand_name = split_line[BRAND_NAME_IDX]
    pl.blend_name = split_line[BLEND_NAME_IDX]
    pl.blend_body = split_line[BLEND_BODY_IDX]
    pl.blend_country = split_line[BLEND_COUNTRY_IDX]
    pl.vitola_name = split_line[VITOLA_NAME_IDX]
    pl.vitola_length = split_line[VITOLA_LENGTH_IDX].strip.to_f.round(2)
    pl.vitola_gauge = split_line[VITOLA_GAUGE_IDX].strip.to_i
    pl.wrapper_name = split_line[WRAPPER_NAME_IDX]
    pl.inventory_count = split_line[INVENTORY_COUNT_IDX].strip.to_i
    pl.smoke_score = split_line[SMOKE_SCORE_IDX].strip.to_i
    pl.smoke_notes = split_line[SMOKE_NOTES_IDX]
    pl.cigar_rating = split_line[CIGAR_RATING_IDX]
    pl.order_id = split_line[FK_ORDER_ID_IDX].strip.to_i
    pl.price_per_stick= split_line[PRICE_PER_STICK_IDX][1..].strip.to_f.round(2)
    pl.purchase_count = split_line[PURCHASE_COUNT_IDX].strip.to_i
    pl.humidor_name = split_line[HUMIDOR_NAME_IDX]
    lots << pl
  end
  lots
end
