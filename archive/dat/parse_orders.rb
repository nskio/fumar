require 'fumar/models'

ORDER_ID_IDX = 0
PURCHASE_DATE_IDX= 1
VENDOR_NAME_IDX = 2
SHIPPING_COST_IDX = 7
TAX_COST_IDX = 8
TOTAL_PRICE_IDX = 9

def parse_orders(log, file)
  orders = []

  File.readlines(file).each do |line|
    current_order = {}
    split_line = line.split(/\t/)
    order_id = split_line[ORDER_ID_IDX]
    is_new_order = order_id != ""

    if is_new_order
      current_order[:order_id] = order_id
      purchase_date = split_line[PURCHASE_DATE_IDX]
      current_order[:purchase_date] = DateTime.strptime(purchase_date,'%m/%d/%Y')
      vendor_name = split_line[VENDOR_NAME_IDX]
      current_order[:product_total] = 0
      current_order[:vendor_name] = vendor_name
      orders << current_order
    end

    shipping_cost = split_line[SHIPPING_COST_IDX]
    has_shipping_cost = shipping_cost != ""
    tax_cost = split_line[TAX_COST_IDX]
    has_tax_cost = tax_cost != ""
    total_price = split_line[TOTAL_PRICE_IDX]
    has_total_price = total_price != ""

    finalize_order = has_shipping_cost && has_tax_cost && has_total_price

    if finalize_order
      orders.last[:shipping_cost] = shipping_cost
      orders.last[:tax_cost] = tax_cost
    end

    product_total = split_line[5][1..]
    orders.last[:product_total] = orders.last[:product_total] + product_total.to_f
  end

  orders
end
