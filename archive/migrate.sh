#!/bin/bash
_dir="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
source $_dir/config.sh

defaultEnv=dev
env=$1

if [[ "$env" == "" ]]; then
  env=$defaultEnv
fi

case $env in

  "dev")
    echo "Fumar::Migrate::Loading dev"
    export DB_TYPE=sqlite
    export DB_FILE=$_dir/$DEV_DB_FILE_NAME
    ;;

  "prod")
    echo "Fumar::Migrate::Loading prod"
    ;;

  *)
    echo "ERROR: Unknown environment, failing to start."
    exit 1
    ;;
esac

echo "Fumar::Environment: [ $env ]"
export environment=$env
$_dir/migrate.rb
