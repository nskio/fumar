require 'yaml'
require 'open3'

module Importers
  class OrderDatImporter
    def initialize(orders_dir_path, user)
      @orders_dir_path = orders_dir_path
      @log = Application['logger']
      @order_file_paths = Dir.entries(@orders_dir_path).filter_map do |f|
        should_include =
          !f.include?('template') &&
          !f.include?('schema') &&
          /yml$/.match?(f)
        File.join(@orders_dir_path, f) if should_include
      end

      @validation_script = File.join(@orders_dir_path, 'validate.sh')
      @valid_order_files = []
      @invalid_order_files = []
      @invalid_order_output = []
      @skipped_order_files = []

      @mh = ModelHelper.new(@log)
      @user = user
    end

    def run()
      @log.info 'OrderDatImporter::load'
      @log.info "orders_dir_path: #{@orders_dir_path}"

      self.validate_order_files
      all_orders = self.read_orders
      raw_orders_to_import = all_orders.map{|o| self.inject_should_import(o)}

      non_existing_raw_orders = raw_orders_to_import.filter{|o| o[:should_import]}
      existing_orders = raw_orders_to_import.filter{|o| !o[:should_import]}

      processed_file_sum =
        @skipped_order_files.length +
        @valid_order_files.length +
        @invalid_order_files.length

      @log.info "============================================================"
      @log.info "PRE-IMPORT REPORT"
      @log.info "============================================================"
      @log.info "Total order files read: #{@order_file_paths.length}"
      @log.info "Orders marked skip: #{@skipped_order_files.length}"
      @log.info "Valid order files: #{@valid_order_files.length}"
      @log.info "Invalid order files: #{@invalid_order_files.length}"
      @log.info "Processed file sum: #{processed_file_sum}"
      @log.info "Existing order count (already in DB): #{existing_orders.length}"
      @log.info "Orders attempting to import: #{non_existing_raw_orders.length}"
      @log.info "============================================================"

      @log.info "============================================================"
      @log.info "SKIPPED_FILES"
      @log.info "============================================================"
      @skipped_order_files.each do |f|
        @log.info "-> #{f}"
      end

      if @invalid_order_files.length > 0
        @log.info "============================================================"
        @log.info "INVALID ORDER OUTPUT"
        @log.info "============================================================"
        @invalid_order_output.each do |i|
          @log.info i[:file]
          @log.info i[:output]
        end
      end

      self.save_orders non_existing_raw_orders
    end

    private

    def validate_order_files
      @order_file_paths.each do |of|
        # Don't even attempt to validate skipped files
        order_content = YAML.load(File.read(of))
        if order_content.key?('skip') && order_content['skip'] == true
          @skipped_order_files << of
          next
        end

        stdout, stderr, status = Open3.capture3("#{@validation_script} #{of}")

        if status.success?
          @valid_order_files << of
        else
          @invalid_order_files << of
          @invalid_order_output << {
            file: of,
            output: stdout,
          }
        end
      end
    end

    def read_orders
      @valid_order_files.map do |order_file_path|
        @log.debug "Reading #{order_file_path}"
        YAML.load(File.read(order_file_path))
      end
    end

    def save_orders(orders)
      @log.info "OrderDatImporter::save_orders"

      orders.each do |o|
        if o.key?('skip') && o['skip'] != nil
          next
        end

        purchase_date = DateTime.strptime(o['purchase_date'].to_s, '%m%d%Y')
        @log.info "purchase_date: #{o['purchase_date']}"
        @log.info "arrival_date: [#{o['arrival_date']}]"
        if o.key?('arrival_date') && o['arrival_date'] != nil
          arrival_date = DateTime.strptime(o['arrival_date'].to_s, '%m%d%Y')
        else
          next # Skip the order if it hasn't arrived yet
        end

        vendor = @mh.find_or_create_vendor(o['vendor_name'], nil) #TODO: link lookup

        order_total = o['products'].reduce(0){|sum, p| sum + p['price']} +
          o['shipping_cost'] + o['tax_cost']
        if o.key?('discount') && o['discount'] != nil
          order_total -= o['discount']
        end

        order = Order.create(
          purchase_date: purchase_date,
          shipping_cost: o['shipping_cost'].to_f.round(2),
          tax_cost: o['tax_cost'].to_f.round(2),
          order_total: order_total,
          vendor: vendor,
          user: @user,
        )

        o['products'].each do |p|
          product = Product.create(
            name: p['name'],
            order: order,
            price: p['price'],
            purchase_count: p['quantity'],
          )

          p['lots'].each do |l|
            brand = @mh.find_or_create_brand(l['brand'])
            blend = @mh.find_or_create_blend(
              l['blend_name'], l['blend_body'], l['blend_country'], brand
            )
            vitola = @mh.find_or_create_vitola(
              l['vitola_name'], l['vitola_length'], l['vitola_gauge']
            )
            wrapper = @mh.find_or_create_wrapper(l['wrapper_name'])
            cigar = @mh.find_or_create_cigar(blend, vitola, wrapper)

            purchase_lot = @mh.find_or_create_purchase_lot(
              purchase_date, arrival_date, l['count'], l['count'],
              cigar, product, nil # TODO: Leaving off humidors for now
            )
          end
        end
      end
    end

    def inject_should_import(raw_order)
      disk_order_date = DateTime.strptime(raw_order['purchase_date'].to_s, '%m%d%Y')
      raw_order[:should_import] = !@mh.order_exists?(
        disk_order_date,
        raw_order['vendor_name'],
        raw_order['products'].map{|p| p['name']},
      )
      raw_order
    end
  end
end
