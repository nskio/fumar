module Importers
  class SmokeDatImporter
    def initialize(smokes_dir_path)
      @smokes_dir_path = smokes_dir_path
      @log = Application['logger']
      @smoke_file_paths = Dir.entries(@smokes_dir_path).filter_map do |f|
        File.join(@smokes_dir_path, f) if !f.include? 'template'
      end
    end

    def run()
      @log.info 'SmokeDatImporter::load'
      @log.info "orders_dir_path: #{@orders_dir_path}"
      @log.info @smoke_file_paths
    end
  end
end
