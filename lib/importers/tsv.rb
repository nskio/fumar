require 'pry-byebug'

module Importers

  class Tsv
    def initialize(log, user, order_file, lot_file)
      @log = log
      @user = user
      @order_file = order_file
      @lot_file = lot_file

      @vendor_link_map = {
        'Cigars Daily' => 'https://cigarsdaily.com',
        'Famous Smoke' => 'https://www.famous-smoke.com',
        'Atlantic Cigar Company' => 'https://atlanticcigar.com',
        'Cigar King' => 'https://www.cigarking.com',
        'Cigar Bid' => 'https://www.cigarbid.com',
        'Watch City Cigar' => 'https://www.watchcitycigar.com',
        'Small Batch Cigar' => 'https://www.smallbatchcigar.com',
      }

      @mh = ModelHelper.new(@log)
    end

    def run()
      @log.debug "TsvImporter::run"

      @log.info "Parsing orders"
      parsed_orders = Importers::parse_orders(@log, @order_file)
      @log.info "Parsed #{parsed_orders.length} orders"

      @log.info "Saving orders and related vendors to database"
      self.save_orders parsed_orders

      @log.debug "Parsing purchase lots"
      parsed_purchase_lots = Importers::parse_purchase_lots(@log, @lot_file)
      @log.info "Parsed #{parsed_purchase_lots.length} purchase lots"

      @log.info "Saving purchase lots and related vendors to database"
      self.save_purchase_lots parsed_purchase_lots
    end

    private

    def save_orders(parsed_orders)
      imported_orders = []
      parsed_orders.each do |o|
        vendor_name = o[:vendor_name]
        vendor = Vendor.find(name: vendor_name)
        if not vendor
          @log.info "Vendor not found [ #{vendor_name} ], creating new one"
          vendor_link = @vendor_link_map[vendor_name]
          vendor = Vendor.create(
            name: vendor_name,
            link: vendor_link,
          )
        end

        imported_product_names = o[:products].map{|p| p[:name] }
        order_exists = @mh.order_exists?(
          o[:purchase_date], vendor_name, imported_product_names)
        if !order_exists
          @log.debug "Could not find order with pdate: #{o[:purchase_date]}, creating and saving"
          order = Order.create({
            purchase_date: o[:purchase_date],
            order_total: o[:order_total].round(2),
            shipping_cost: o[:shipping_cost].to_f.round(2),
            tax_cost: o[:tax_cost].to_f.round(2),
            user: @user,
            vendor: vendor,
          })
        else
          @log.debug "Order seems to already exist: #{o}, skipping."
          next
        end

        @log.info "============================================================"
        @log.info "Beginning product import for order ID: #{order.id}"
        @log.info "============================================================"
        # Figure out what products don't already exist for this order by
        # checking to see which potential imported products are already associated
        # with this order based on their name.
        @log.debug 'imported_product_names:'
        @log.debug imported_product_names
        product_ds = Product.where do
          Sequel.&(
            {name: imported_product_names},
            {order_id: o[:order_id].to_i},
          )
        end
        @log.debug 'existing_product_names'
        existing_product_names = product_ds.map{|p| p.name}
        @log.debug existing_product_names

        @log.debug 'names_to_import'
        names_to_import =
          Set.new(imported_product_names) - Set.new(existing_product_names)
        @log.debug names_to_import

        if names_to_import.length != 0
          @log.info "Executing import of #{names_to_import.length} products:"
          @log.info names_to_import

          Product.import(
            [:name, :order_id, :price, :purchase_count],
            o[:products].map{|p| [p[:name], o[:order_id].to_i, p[:price], p[:count]]},
          )
        else
          @log.info 'No names to import! Continuing.'
        end

        imported_orders << order
      end

      @log.info "Saved #{imported_orders.length} orders"
      @log.debug "Order summary:"
      imported_orders.each { |o| @log.debug "Date: #{o.purchase_date}, Vendor: #{o.vendor.name}, Total: $#{o.total_price}, ID: #{o.id}" }
      imported_orders
    end

    def save_purchase_lots(parsed_purchase_lots)
      imported_purchase_lots = []
      parsed_purchase_lots.each do |pl|
        mh = ModelHelper.new(@log)

        brand = mh.find_or_create_brand(pl.brand_name)

        blend = mh.find_or_create_blend(
          pl.blend_name, pl.blend_body, pl.blend_country, brand,
        )

        vitola = mh.find_or_create_vitola(
          pl.vitola_name, pl.vitola_length, pl.vitola_gauge,
        )

        wrapper = mh.find_or_create_wrapper(pl.wrapper_name)

        humidor_names = pl.humidor_name.split(/,/)
        pl_humidors = []
        humidor_names.each do |hn|
          humidor = Humidor.find(name: hn)
          if not humidor
            @log.info "Humidor not found [ #{pl.humidor_name} ], creating new one"
            humidor = Humidor.create(
              name: hn,
              user: @user,
            )
          end
          pl_humidors << humidor
        end

        cigar = mh.find_or_create_cigar(blend, vitola, wrapper)

        purchase_lot = mh.find_or_create_purchase_lot(
          pl.purchase_date, pl.arrival_date, pl.purchase_count, pl.inventory_count,
          cigar, Product.find(id: pl.product_id), pl_humidors,
        )
      end

      @log.info "Saved #{::PurchaseLot.count} purchase lots"
    end
  end
end
