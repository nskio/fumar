module Importers

################################################################################
# parse_orders
################################################################################
  ORDER_ID_IDX = 0
  PURCHASE_DATE_IDX= 1
  VENDOR_NAME_IDX = 2
  PRODUCT_NAME_IDX = 4
  PRODUCT_COUNT_IDX = 5
  PRODUCT_COST_IDX = 6
  SHIPPING_COST_IDX = 8
  TAX_COST_IDX = 9
  TOTAL_PRICE_IDX = 10

  def self.parse_orders(log, file)
    orders = []

    File.readlines(file).each do |line|
      current_order = {}
      split_line = line.split(/\t/)
      order_id = split_line[Importers::ORDER_ID_IDX]
      is_new_order = order_id != ""

      if is_new_order
        current_order[:order_id] = order_id
        purchase_date = split_line[PURCHASE_DATE_IDX]
        current_order[:purchase_date] = DateTime.strptime(purchase_date,'%m/%d/%Y')
        vendor_name = split_line[VENDOR_NAME_IDX]
        current_order[:order_total] = 0
        current_order[:vendor_name] = vendor_name
        current_order[:products] = [{
          name: split_line[PRODUCT_NAME_IDX],
          price: split_line[PRODUCT_COST_IDX][1..].to_f,
          count: split_line[PRODUCT_COUNT_IDX].to_i,
        }]
        orders << current_order
      else
        orders.last[:products] << {
          name: split_line[PRODUCT_NAME_IDX],
          price: split_line[PRODUCT_COST_IDX][1..].to_f,
          count: split_line[PRODUCT_COUNT_IDX].to_i,
        }
      end

      order_total = orders.last[:products].last[:cost]
      orders.last[:order_total] = orders.last[:order_total] + order_total.to_f

      shipping_cost = split_line[Importers::SHIPPING_COST_IDX]
      has_shipping_cost = shipping_cost != ""
      tax_cost = split_line[Importers::TAX_COST_IDX]
      has_tax_cost = tax_cost != ""
      total_price = split_line[Importers::TOTAL_PRICE_IDX]
      has_total_price = total_price != ""

      finalize_order = has_shipping_cost && has_tax_cost && has_total_price

      if finalize_order
        orders.last[:shipping_cost] = shipping_cost
        orders.last[:tax_cost] = tax_cost
      end

      orders.last[:order_total] =
        (orders.last[:order_total].round(2) + shipping_cost.to_f.round(2) + tax_cost.to_f.round(2))
    end

    orders
  end
  #
################################################################################
# parse_purchase_lots
################################################################################

  PL_PURCHASE_DATE_IDX = 0
  ARRIVAL_DATE_IDX = 1
  CIGAR_LINK_IDX = 3 # TODO
  #VENDOR_NAME_IDX = 4 # Not necessary, comes off the order?
  BRAND_NAME_IDX = 5
  BLEND_NAME_IDX = 6
  BLEND_BODY_IDX = 7
  BLEND_COUNTRY_IDX = 9
  VITOLA_NAME_IDX = 8
  VITOLA_LENGTH_IDX = 11
  VITOLA_GAUGE_IDX = 12
  WRAPPER_NAME_IDX = 10
  INVENTORY_COUNT_IDX = 14
  HUMIDOR_NAME_IDX = 15
  SMOKE_SCORE_IDX = 17
  SMOKE_NOTES_IDX = 19
  CIGAR_RATING_IDX = 18
  FK_PRODUCT_ID_IDX = 20
  PURCHASE_COUNT_IDX = 21
  PRICE_PER_STICK_IDX= 13

  class PurchaseLot
    attr_accessor \
      :purchase_date,
      :arrival_date,
      :cigar_link,
      :vendor_name,
      :brand_name,
      :blend_name,
      :blend_body,
      :vitola_name,
      :blend_country,
      :wrapper_name,
      :vitola_length,
      :vitola_gauge,
      :inventory_count,
      :purchase_count,
      :price_per_stick,
      :humidor_name,
      :smoke_score,
      :cigar_rating,
      :smoke_notes,
      :product_id
  end

  def self.parse_purchase_lots(log, file)
    lots = []
    File.readlines(file).each do |line|
      pl = Importers::PurchaseLot.new
      split_line = line.split(/\t/)
      purchase_date = split_line[Importers::PL_PURCHASE_DATE_IDX]
      pl.purchase_date = DateTime.strptime(purchase_date,'%m/%d/%Y')
      arrival_date = split_line[Importers::ARRIVAL_DATE_IDX]
      pl.arrival_date = arrival_date == "" ?
        nil :
        DateTime.strptime(arrival_date,'%m/%d/%Y')
      pl.brand_name = split_line[Importers::BRAND_NAME_IDX]
      pl.blend_name = split_line[Importers::BLEND_NAME_IDX]
      pl.blend_body = split_line[Importers::BLEND_BODY_IDX]
      pl.blend_country = split_line[Importers::BLEND_COUNTRY_IDX]
      pl.vitola_name = split_line[Importers::VITOLA_NAME_IDX]
      pl.vitola_length = split_line[Importers::VITOLA_LENGTH_IDX].strip.to_f.round(2)
      pl.vitola_gauge = split_line[Importers::VITOLA_GAUGE_IDX].strip.to_i
      pl.wrapper_name = split_line[Importers::WRAPPER_NAME_IDX]
      pl.inventory_count = split_line[Importers::INVENTORY_COUNT_IDX].strip.to_i
      pl.smoke_score = split_line[Importers::SMOKE_SCORE_IDX].strip.to_i
      pl.smoke_notes = split_line[Importers::SMOKE_NOTES_IDX]
      pl.cigar_rating = split_line[Importers::CIGAR_RATING_IDX]
      pl.product_id = split_line[Importers::FK_PRODUCT_ID_IDX].strip.to_i
      pl.price_per_stick= split_line[Importers::PRICE_PER_STICK_IDX][1..].strip.to_f.round(2)
      pl.purchase_count = split_line[Importers::PURCHASE_COUNT_IDX].strip.to_i
      pl.humidor_name = split_line[Importers::HUMIDOR_NAME_IDX]
      lots << pl
    end
    lots
  end

end
