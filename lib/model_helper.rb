class ModelHelper
  def initialize(log)
    @log = log
  end

  def find_or_create_vendor(name, link)
    vendor = Vendor.find(name: name)
    if not vendor
      @log.info "Vendor not found [ #{name} ], creating new one"
      vendor = Vendor.create(name: name, link: link)
    end
    vendor
  end

  def find_or_create_brand(brand_name)
    brand = Brand.find(name: brand_name)
    if not brand
      @log.info "Brand not found [ #{brand_name} ], creating new one"
      brand = Brand.create(name: brand_name)
    end
    brand
  end

  def find_or_create_blend(name, body, country, brand)
    blend = Blend.find(name: name, brand: brand)
    if not blend
      @log.info "Blend not found [ #{name} ], creating new one"
      blend = Blend.create(
        name: name,
        body: body,
        country: country,
        brand: brand,
      )
    end
    blend
  end

  def find_or_create_vitola(name, length, gauge)
    vitola = Vitola.find(name: name)
    if not vitola
      @log.info "Vitola not found [ #{name} ], creating new one"
      vitola = Vitola.create(name: name, length: length, gauge: gauge)
    end
    vitola
  end

  def find_or_create_wrapper(name)
    wrapper = Wrapper.find(name: name)
    if not wrapper
      @log.info "Wrapper not found [ #{name} ], creating new one"
      wrapper = Wrapper.create(name: name)
    end
    wrapper
  end

  def find_or_create_cigar(blend, vitola, wrapper)
    cigar = Cigar.find(blend: blend, vitola: vitola, wrapper: wrapper)
    if not cigar
      @log.info "Cigar [ #{blend.brand.name} #{blend.name} #{wrapper.name} #{vitola.name} ] not found, creating new one"
      cigar = Cigar.create(blend: blend, vitola: vitola, wrapper: wrapper)
    end
    cigar
  end

  def find_or_create_purchase_lot(purchase_date, arrival_date, purchase_count,
                                   inventory_count, cigar, product, humidors)
    # Check to see if this purchase lot already exists based on the purchase
    # date and the cigar itself. NOTE: This is making an assumption you
    # aren't buying the same cigar more than once in a given day, which is
    # really a heuristic that could pretty easily fall flat, but should be
    # alright for my purposes of import
    existing_pl_count = ::PurchaseLot.where do
      Sequel.&(
        {purchase_date: purchase_date},
        {cigar: cigar},
      )
    end.count

    if existing_pl_count == 0
      purchase_lot_hash = {
        purchase_date: purchase_date,
        purchase_count: purchase_count,
        inventory_count: inventory_count,
        product: product,
        cigar: cigar,
      }

      if arrival_date
        purchase_lot_hash[:arrival_date] = arrival_date
      end

      purchase_lot = ::PurchaseLot.create(purchase_lot_hash)

      if humidors != nil
        humidors.each do |h|
          @log.debug "Attempting to associate humidor with name and id: [#{h.name} | #{h.id}]"
          purchase_lot.add_humidor(h)
        end
      end

      purchase_lot.save
    end

    purchase_lot
  end

  def find_or_create_order(purchase_date, order_total, shipping_cost, tax_cost,
                          user, vendor)
  end

  def order_exists?(purchase_date, vendor_name, order_product_names)
      date_matching_orders = Order.where(purchase_date: purchase_date)

      # We know we need to import it if there are no matching orders that match
      # the date
      return false if date_matching_orders.count == 0

      # If there are date matching orders, check to see if there are any orders
      # that day that were from the same vendor as the order that we're trying
      # to import. If there aren't, then we know that we also have to import.
      return false if date_matching_orders.all.select do |o|
        o.vendor.name == vendor_name
      end.count == 0

      # If there were more than one order made on the same day from the same
      # vendor, now we need to check if there is a set intersection between
      # the purchase lots on the matching orders, and the purchase lots on
      # the order that we're considering for import
      # TODO: We're missing the fundamental model that represents an order's
      # product line item to be able to do this set comparison.
      matching_existing_order = date_matching_orders.all.select do |db_order|
        db_product_names = db_order.products.map{|p| p.name}
        product_name_diff = Set.new(order_product_names) - Set.new(db_product_names)

        # If the set difference is nothing, we know these sets perfectly overlap,
        # and therefore the order has already been imported.
        product_name_diff.length == 0
      end

      # We found a matching order already in the database, so mark it to skip
      # the import, else if it isn't found, we can safely assume that this order
      # doesn't already exist
      order_exists = matching_existing_order != nil
      return order_exists
  end

end
